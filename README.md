# Bitbucket OAuth 2 Client

This is a small standalone script to make OAuth 2 calls to the Bitbucket API
using the Authorization Code Grant flow in a way native/mobile apps would
integrate.


## Installation / Setup

[Create an OAuth consumer](https://confluence.atlassian.com/x/pwIwDg) on
Bitbucket with the desired permission scopes and configure a callback URL on
`localhost` (e.g. `http://localhost:12345`).

Next, create a Python virtual environment and install the dependencies:

    $ mkvirtualenv bboauth2
    (bboauth2)$ pip install -r requirements.txt


## Usage

We can now let the program call the Bitbucket API on behalf of your personal
account by supplying the consumer's client id and secret, the TCP port
configured in the consumer's localhost callback URL and specifying a REST
endpoint we want to hit (in this case the current user endpoint that will show
the user account represented by the access token:

    (bboauth2)$ python oauth2.py \
      --client_id=HjrjuR89bL6CLYUs9r \
      --secret=MVBLZyYRAtkaq5SCensd2k6Tw8LeKVUh \
      -p 12345 \
      https://api.bitbucket.org/2.0/user

The script will open a browser window to Bitbucket asking the user the login
and approve the issuance of an access token. Bitbucket then redirects the
browser to the callback URL (http://localhost:12345 in our case) where our
script is running an ad-hoc webserver to receive the authorization code.

It then exchanges the authorization code for a refresh token and access token
by making a call to `https://bitbucket.org/site/oauth2/access_token` as per
[RFC-6749](https://tools.ietf.org/html/rfc6749#section-4.1.3).

Lastly it hits the specified REST API (`https://api.bitbucket.org/2.0/user` in
out example), passing along the access token in the Authorization request
header:

    (bboauth2)$ python oauth2.py \
      --client_id=HjrjuR89bL6CLYUs9r \
      --secret=MVBLZyYRAtkaq5SCensd2k6Tw8LeKVUh \
      -p 12345 \
      https://api.bitbucket.org/2.0/user

    Waiting for callback at http://localhost:12345/ ...
    {
      "username": "evzijst", 
      "website": "", 
      "display_name": "Erik van Zijst", 
      "uuid": "{a288a0ab-e13b-43f0-a689-c4ef0a249875}", 
      "links": {
        "hooks": {
          "href": "https://api.bitbucket.org/internal/users/evzijst/hooks"
        }, 
        "self": {
          "href": "https://api.bitbucket.org/2.0/users/evzijst"
        }, 
        "repositories": {
          "href": "https://api.bitbucket.org/2.0/repositories/evzijst"
        }, 
        "html": {
          "href": "https://bitbucket.org/evzijst/"
        }, 
        "followers": {
          "href": "https://api.bitbucket.org/2.0/users/evzijst/followers"
        }, 
        "avatar": {
          "href": "https://bitbucket.org/account/evzijst/avatar/32/"
        }, 
        "following": {
          "href": "https://api.bitbucket.org/2.0/users/evzijst/following"
        }, 
        "snippets": {
          "href": "https://api.bitbucket.org/2.0/snippets/evzijst"
        }
      }, 
      "created_on": "2010-07-07T05:16:36+00:00", 
      "location": "@erikvanzijst", 
      "type": "user"
    }
